 #include <stdio.h>
#include <stdlib.h>

void main() {
    int filas, columnas, fil, col;
    int encontrarColumna = 0, encontrarFila = 0, temp, tempFila, tempColumna;
    printf("Cuantas filas quiere para la matriz?\n");
    scanf("%d", &filas);

    printf("Cuantas columnas quiere para la matriz?\n");
    scanf("%d", &columnas);

    int m[filas][columnas];

    //Aqui se ingresan los valores de la matriz
    for (fil = 0; fil < filas; fil++) {
        for (col = 0; col < columnas; col++) {
            printf("Ingrese los datos para la posición (%d,%d)\n", fil + 1, col + 1);
            scanf("%d", &m[fil][col]);
        }
    }

    //Muestro la matriz ingresada
    for (fil = 0; fil < filas; fil++) {
        printf("\n");
        for (col = 0; col < columnas; col++) {
            printf("%d\t", m[fil][col]);
        }
    }

    for (fil = 0; fil < filas; fil++) {
        for (col = 0; col < columnas; col++) {
            temp = m[fil][col];
            for (tempColumna = 0; tempColumna < columnas; tempColumna++) {      
                if (temp > m[fil][tempColumna]) {
                    encontrarColumna=encontrarColumna+1;
                }
            }
            for (tempFila = 0; tempFila < filas; tempFila++) {
                if (temp < m[tempFila][col]) {
                    encontrarFila=encontrarFila+1;
                }
            }
            if (encontrarColumna == 0 && encontrarFila == 0) {
                printf("\nSe ha encontrado un punto de silla en la matriz, y es: %d, que se encuentra en la fila: %d y la columna: %d\n", temp, (fil + 1), (col + 1));
            } 
            encontrarFila=0;
            encontrarColumna=0;
        }
    }
}
